# Ansible playbook for installing Docker

## Create virtual environment

* As good practice we will install ansible in a virtual environment:

```bash
python -m venv ansible
source ansible/bin/activate
```

## Install ansible

* Make sure you have pip installed:

```bash
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
```

* Then install ansible:

```bash
pip install ansible
```

## Install Ansible-Galaxy Role for Docker

* For we are not the first ones trying to do this, there are already elaborated Ansible-Roles out there, e.g.:
<https://galaxy.ansible.com/geerlingguy/docker>

* We will use this role using `ansible-galaxy`-command:

```bash
ansible-galaxy install geerlingguy.docker
```

* This will install the docker role into some ansible-role-directory managed by ansible.

* It allows us to use the docker-role as easy as 1-2-3!


## Usage of Ansible-Galaxy Roles

* go to your playbook-file, e.g. `docker.yml`:

```yaml
---
- hosts: docker_vm
  roles:
    - geerlingguy.docker
  become: yes
```

* as you can see, we can use the roles downloaded via `ansible-galaxy`-command just like this! Great!

* now you just have to adjust your `inventory.txt`-file and add the right ip-address.
* in case your ssh-key is not on the server, you may have to add `ansible_ssh_pass=your-awesome-password`

## Run Roles

* Run your role with:

```bash
ansible-playbook docker.yml -i inventory.txt
```

## Start a docker-container

If you also want to start a docker container edit your `docker.yml`-file like this:

```yaml
---
- hosts: docker_vm

  vars:
    pip_package: python3-pip
    pip_executable: pip3
    pip_install_packages:
      - name: docker

  roles:
    - geerlingguy.pip
    - geerlingguy.docker
  become: yes

  tasks:
    - name: Create a data container
      docker_container:
        name: webserver
        image: nginx
```

Then run the playbook with the command mentioned above.
